var TxtType = function(el, toRotate, period) {
    this.toRotate = toRotate;
    this.el = el;
    this.loopNum = 0;
    this.period = parseInt(period, 10) || 2000;
    this.txt = '';
    this.tick();
    this.isDeleting = false;
};

TxtType.prototype.tick = function() {
    var i = this.loopNum % this.toRotate.length;
    var fullTxt = this.toRotate[i];

    if (this.isDeleting) {
    this.txt = fullTxt.substring(0, this.txt.length - 1);
    } else {
    this.txt = fullTxt.substring(0, this.txt.length + 1);
    }

    this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

    var that = this;
    var delta = 200 - Math.random() * 100;

    if (this.isDeleting) { delta /= 2; }

    if (!this.isDeleting && this.txt === fullTxt) {
    delta = this.period;
    this.isDeleting = true;
    } else if (this.isDeleting && this.txt === '') {
    this.isDeleting = false;
    this.loopNum++;
    delta = 500;
    }

    setTimeout(function() {
    that.tick();
    }, delta);
};

window.onload = function() {
    var elements = document.getElementsByClassName('typewrite');
    for (var i=0; i<elements.length; i++) {
        var toRotate = elements[i].getAttribute('data-type');
        var period = elements[i].getAttribute('data-period');
        if (toRotate) {
          new TxtType(elements[i], JSON.parse(toRotate), period);
        }
    }
    // INJECT CSS
    var css = document.createElement("style");
    css.type = "text/css";
    css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
    document.body.appendChild(css);
};


var sender = true;

$('textarea').keypress(function(e){
    if(e.keyCode == 13 && !e.shiftKey) {
        var c = String.fromCharCode(e.which);
        var textValue = $('textarea').val();
        var fulltext = textValue + c;
        $('textarea').val('');
    
        if(sender){
            $('.msg-insert').append('<p class="msg-send">' + fulltext + '</p>');
            sender = false;
        }else{
            $('.msg-insert').append('<p class="msg-receive">' + fulltext + '</p>');
            sender = true;
        }

        e.preventDefault();
    };
});

$('.chat-head').click(function(){
    $('.chat-body').toggle();
})

var print = document.getElementById('print');
var erase = false;

var go = function(x) {
    if (x === 'ac') {
    /* implemetnasi clear all*/
        print.value = '';
        erase = true;
    } else if (x === 'eval') {
        print.value = Math.round(evil(print.value) * 10000) / 10000;
        erase = true;
    } else if (x === 'log'|| x === 'sin'|| x === 'tan'){
        switch(x){
            case 'log': print.value = Math.log10(print.value);
            break;
            case 'sin': print.value = Math.sin(print.value);
            break;
            case 'tan': print.value = Math.tan(print.value);
            break;  
        }
    } else {
        print.value += x;
    }
};
     
function evil(fn) {
    return new Function('return ' + fn)();
}


 var themes = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"} 
];


 localStorage.setItem("themes",JSON.stringify(themes));
 var catalog = JSON.parse(localStorage.getItem("themes"));

$.each(catalog, function(index){
    var id = catalog[index].id;
    var Color = catalog[index].text;
    var bgColor = catalog[index].bcgColor;
    var fontColor = catalog[index].fontColor;

    localStorage.setItem(id,Color.concat(",",bgColor,",",fontColor));
})



$(document).ready(function() {
    
    $('.my-select').select2({
        placeholder : "Select Color",
        data : themes
    });


    if(localStorage.getItem("selectedItem") == null){
        localStorage.setItem("selectedItem",3);
        var themeDefault = localStorage.getItem(localStorage.getItem("selectedItem"));
        var applyDefault = themeDefault.split(",");
        
        document.body.style.backgroundColor = applyDefault[1];
        document.getElementsByClassName('typewrite')[0].style.color = applyDefault[2];
    }else{
        var themeSaved = localStorage.getItem(localStorage.getItem("selectedItem"));
        var applySaved = themeSaved.split(",");
        
        document.body.style.backgroundColor = applySaved[1];
        document.getElementsByClassName('typewrite')[0].style.color = applySaved[2];
    }


    $('.apply-button').on('click', function(){  // sesuaikan class button
            var theme = localStorage.getItem($(".my-select").val());
            var apply = theme.split(",");
        
            // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
            document.body.style.backgroundColor = apply[1];
            document.getElementsByClassName('typewrite')[0].style.color = apply[2]; 
        
            // [TODO] simpan object theme tadi ke local storage selectedTheme
        
            localStorage.setItem("selectedItem",$(".my-select").val());

            return false;
        });

});






