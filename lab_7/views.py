from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import Friend
from lab_9.csui_helper import get_mahasiswa_list, get_auth_param_dict
import os
import json
import math

response = {}

def index(request):
	# Page halaman menampilkan list mahasiswa yang ada
	# TODO berikan akses token dari backend dengan menggunakaan helper yang ada
	if 'user_login' in request.session:
		page = request.GET.get('page', 1) if int(request.GET.get('page', 1)) > 0 and int(request.GET.get('page', 1)) < 69 else 1

		mahasiswa_list = get_mahasiswa_list(request.session['access_token'], page)[0]
		total_page = math.ceil(get_mahasiswa_list(request.session['access_token'], page)[1]/100)
		friend_list = Friend.objects.all()
		auth = get_auth_param_dict(request.session['access_token'])

		response["mahasiswa_list"] = mahasiswa_list
		response["total_page"] = total_page
		response["friend_list"] = friend_list
		response["auth"] = auth
		response["page"] = page

		html = 'lab_7/lab_7.html'
		return render(request, html, response)
	else:
		html = 'lab_9/session/login.html'
		return render(request, html, response)

def friend_list(request):
	if 'user_login' in request.session:
		friend_list = Friend.objects.all()
		response['friend_list'] = friend_list
		html = 'lab_7/daftar_teman.html'
		return render(request, html, response)
	else:
		html = 'lab_9/session/login.html'
		return render(request, html, response)


def get_friend_list(request):
    if request.method == 'GET':
        friends = [obj.as_dict() for obj in Friend.objects.all()]
        return JsonResponse({"results": friends}, content_type='application/json')


@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        address = request.POST['address']
        mail_code = request.POST['mail_code']
        hometown = request.POST['hometown']
        birthday = request.POST['birthday']
        program = request.POST['program']
        angkatan = request.POST['angkatan']
        connect = Friend.objects.filter(npm = npm).exists()
        if(not connect):
            friend = Friend(friend_name=name,npm=npm,address=address,mail_code=mail_code,hometown=hometown,birthday=birthday,program=program,angkatan=angkatan)
            friend.save()
        data = model_to_dict(friend)
        return HttpResponse(data)


def delete_friend(request, npm):
    Friend.objects.filter(npm=npm).delete()
    return HttpResponseRedirect('/lab-7/friend-list/')

def friend_information(request, npm):
	friend = Friend.objects.filter(npm=npm)[0]
	response['friend'] = friend
	html = 'lab_7/info.html'
	return render(request, html, response)

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
         'is_taken': Friend.objects.filter(npm = npm).exists()
    }
    return JsonResponse(data)

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data
